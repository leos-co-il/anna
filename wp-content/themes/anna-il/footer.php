<?php
$facebook = opt('facebook');
$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<?= svg_simple(ICONS.'to-top.svg'); ?>
		</a>
		<div class="foo-form-wrap">
			<div class="container">
				<div class="row justify-content-center align-items-center mb-5">
					<div class="col-xl-10 col-lg-11 col-12">
						<?php if ($f_title = opt('foo_form_title')) : ?>
							<h2 class="foo-form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('foo_form_subtitle')) : ?>
							<h3 class="foo-form-subtitle"><?= $f_text; ?></h3>
						<?php endif;
						getForm('24'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid mt-5">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-12 col-11">
					<div class="row justify-content-sm-between justify-content-center">
						<div class="col-xxl-3 col-lg-auto col-sm-4 col-11 foo-menu main-footer-menu">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'תפריט ראשי //', 'en' => 'Main menu //'], 'he'); ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '1'); ?>
							</div>
						</div>
						<div class="col-lg col-sm-8 col-11 foo-menu links-footer-menu">
							<h3 class="foo-title">
								<?php $title_m = opt('foo_links_menu_title');
								echo $title_m ? $title_m : lang_text(['he' => 'מאמרים חשובים', 'en' => 'Important articles'], 'he'); ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-links-menu', '1', 'two-columns'); ?>
							</div>
						</div>
						<div class="col-lg-auto col-sm-6 col-11 foo-menu contacts-footer-menu">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'פרטי התקשרות // ', 'en' => "Contact information //"], 'he'); ?>
							</h3>
							<div class="menu-border-top contact-menu-foo mb-4">
								<ul class="contact-list d-flex flex-column">
									<?php if ($tel) : ?>
										<li>
											<a href="tel:<?= $tel; ?>" class="contact-info-footer">
												<?= $tel; ?>
											</a>
										</li>
									<?php endif;
									if ($mail) : ?>
										<li>
											<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
												<?= $mail; ?>
											</a>
										</li>
									<?php endif;
									if ($address) : ?>
										<li>
											<a href="https://waze.com/ul?q=<?= $address; ?>"
											   class="contact-info-footer" target="_blank">
												<?= $address; ?>
											</a>
										</li>
									<?php endif;
									if ($open_hours) : ?>
										<li>
									<span>
										<?= $open_hours; ?>
									</span>
										</li>
									<?php endif; ?>
								</ul>
							</div>
							<?php if ($facebook) : ?>
								<a class="facebook-widget" href="<?= $facebook; ?>">
							<span class="foo-title">
								<?= lang_text(['he' => 'עשו לנו לייק // ', 'en' => 'Follow us in Facebook'], 'he'); ?>
							</span>
									<img src="<?= ICONS ?>facebook-like.png" alt="facebook">
								</a>
							<?php endif; ?>
						</div>
						<?php if ($logo = opt('foo_logo')) : ?>
							<div class="col-lg-2 col-sm-6 col-8 foo-logo-col pt-5">
								<a href="/" class="d-flex justify-content-lg-end justify-content-center align-items-center">
									<img src="<?= $logo['url'] ?>" alt="logo" class="w-100">
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="<?= lang_text(['he' => 'לאוס מדיה ואינטראקטיב', 'en' => 'Leos Media and Interactive'], 'he')?>">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="<?= lang_text(['he' => 'קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים', 'en' => 'SEO with Leos Media and Interactive | Website promotion and website building company'], 'he')?>" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
