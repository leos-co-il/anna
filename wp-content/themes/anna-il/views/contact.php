<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
?>
<article class="page-body contact-page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid pt-cont">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-12 col-11">
				<div class="row">
					<div class="col-12">
						<h1 class="base-title contact-base-title"><?php the_title(); ?></h1>
					</div>
					<div class="col-12">
						<div class="base-output">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<div class="row justify-content-between align-items-stretch">
					<div class="col-xl-7 col-lg-6 col-12">
						<div class="contact-form-block contact-page-form">
							<?php getForm('26'); ?>
						</div>
					</div>
					<div class="col-1 col-none-contact"></div>
					<div class="col-lg col-md-10 col-11 contact-col">
						<h3 class="main-title-office">
							<?= lang_text(['he' => 'משרד ראשי', 'en' => 'Main office'], 'he'); ?>
						</h3>
						<?php if ($tel) : ?>
							<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
								<span class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</span>
								<span class="contact-info">
									<h3 class="contact-title">
										<?= lang_text(['he' => 'טלפון רב-קווי:', 'en' => 'Multi-line phone:'], 'he'); ?>
									</h3>
									<span class="contact-type"><?= $tel; ?></span>
								</span>
							</a>
						<?php endif; ?>
						<?php if ($fax) : ?>
							<div class="contact-item wow flipInX" data-wow-delay="0.4s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-fax.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-title">
										<?= lang_text(['he' => 'פקס:', 'en' => 'Fax:'], 'he'); ?>
									</h3>
									<span class="contact-type"><?= $fax; ?></span>
								</div>
							</div>
						<?php endif;
						if ($address) : ?>
							<a href="https://waze.com/ul?q=<?= $address; ?>" class="contact-item wow flipInX" data-wow-delay="0.6s">
								<span class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-geo.png">
								</span>
								<span class="contact-info">
									<span class="contact-title">
										<?= lang_text(['he' => 'כתובתנו:', 'en' => 'Our address:'], 'he'); ?>
									</span>
									<h3 class="contact-type"><?= $address; ?></h3>
								</span>
							</a>
						<?php endif;
						if ($open_hours) : ?>
							<div class="contact-item wow flipInX" data-wow-delay="0.8s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-hours.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-title">
										<?= lang_text(['he' => 'ימים א׳-ה׳:', 'en' => 'Sunday-Thursday:'], 'he'); ?>
									</h3>
									<span class="contact-type"><?= $open_hours; ?></span>
								</div>
							</div>
						<?php endif; ?>
						<?php if (has_post_thumbnail()) : ?>
							<div class="contact-worker">
								<img src="<?= postThumb(); ?>" alt="worker">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
