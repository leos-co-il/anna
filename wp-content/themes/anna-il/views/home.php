<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
if ($fields['h_main_img']) {
	$img = $fields['h_main_img']['url'];
} else {
	$img = has_post_thumbnail() ? postThumb() : '';
}
?>
<section class="main-block-home" <?php if ($img) : ?>
	style="background-image: url('<?= $img; ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="<?= $fields['h_main_img_man'] ? 'col-xl-7 col-lg-8 col-12' : 'col-12'; ?> col-content-home d-flex flex-column justify-content-center align-items-start">
				<?php if ($fields['h_main_text']) : ?>
					<div class="main-home-output">
						<?= $fields['h_main_text']; ?>
					</div>
				<?php endif; ?>
				<?php if ($fields['h_main_link']) : ?>
					<a href="<?= $fields['h_main_link']['url']; ?>" class="base-link home-link">
						<?= lang_text(['he' => 'הקליקו ליצירת קשר', 'en' => 'Click to contact'], 'he') ?>
					</a>
				<?php endif; ?>
			</div>
			<?php if ($fields['h_main_img_man']) : ?>
				<div class="col-xl-5 col-lg-4 col-md-8 col-sm-10 col-12 d-flex justify-content-end align-items-end">
					<img src="<?= $fields['h_main_img_man']['url']; ?>" alt="worker">
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php if ($fields['h_services']) : ?>
	<section class="services-slider-block arrows-slider">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11">
					<div class="services-slider" dir="rtl">
						<?php foreach ($fields['h_services'] as $service) : ?>
							<div class="p-2 slide-item-serv">
								<a class="service-item-wrap" href="<?= isset($service['serv_link']['url']) ? $service['serv_link']['url'] : ''; ?>">
									<span class="service-item-img" <?php if ($service['serv_img']) : ?>
										style="background-image: url('<?= $service['serv_img']['url']; ?>')"
									<?php endif; ?>>
										<span class="post-img-overlay"></span>
									</span>
									<span class="service-title">
										<?= $service['serv_title']; ?>
									</span>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_about_text'] || $fields['h_about_benefits']) : ?>
	<section class="home-about-block home-posts">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-11">
					<div class="row justify-content-between align-items-center">
						<div class="col-xl-5 col-lg-6 col-md-10 col-12 d-flex flex-column align-items-start">
							<div class="base-output about-output">
								<?= $fields['h_about_text']; ?>
							</div>
							<?php if ($fields['h_about_link']) : ?>
								<a href="<?= $fields['h_about_link']['url'];?>" class="base-link">
									<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
											? $fields['h_about_link']['title'] : lang_text(['he' => 'קראו עוד', 'en' => 'Read more'], 'he');
									?>
								</a>
							<?php endif; ?>
						</div>
						<?php if($fields['h_about_img']) : ?>
							<div class="col-xl-5 col-lg-6 about-img-wrap">
								<img src="<?= $fields['h_about_img']['url']; ?>" alt="about-img">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php if ($fields['h_about_benefits']) : ?>
				<div class="row justify-content-center mt-5">
					<div class="col-xl-10 col-lg-11 col-12">
						<div class="row justify-content-center align-items-stretch">
							<?php foreach ($fields['h_about_benefits'] as $benefit) : ?>
								<div class="col-lg-3 col-sm-6 col-12 mb-4">
									<div class="why-item">
										<div class="icon-wrap-why">
											<?php if ($benefit['ben_icon']) : ?>
												<img src="<?= $benefit['ben_icon']['url']; ?>" alt="benefit-icon">
											<?php endif; ?>
										</div>
										<h3 class="why-item-title">
											<?= $benefit['ben_title']; ?>
										</h3>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form');
if ($fields['h_posts']) : ?>
	<section class="home-posts mt-5 mb-5">
		<div class="container">
			<?php if ($fields['h_posts_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="base-title">
							<?= $fields['h_posts_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_posts'] as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
			<?php if ($fields['h_posts_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['h_posts_link']['url']; ?>" class="base-link">
							<?php $about_link = lang_text(['he' => ' לכל המאמרים', 'en' => 'To all articles'], 'he');
							echo (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title']) ?
									$fields['h_posts_link']['title'] : $about_link; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_slider_seo']) : ?>
	<section class="dark-home-slider">
		<?php get_template_part('views/partials/content', 'slider',
				[
						'content' => $fields['h_slider_seo'],
						'img' => $fields['h_slider_img'],
				]); ?>
	</section>
<?php endif;
if ($fields['video_slider']) : ?>
	<div class="videos-block arrows-slider">
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<?php if ($fields['h_video_title']) : ?>
					<div class="col-12">
						<h2 class="base-title">
							<?= $fields['h_video_title']; ?>
						</h2>
					</div>
				<?php endif; ?>
				<div class="col-sm-12 col-11">
					<div class="video-slider" dir="rtl">
						<?php foreach ($fields['video_slider'] as $video) : ?>
							<div class="mb-3 p-2">
								<div class="video-item" style="background-image: url('<?= getYoutubeThumb($video['video_link'])?>')">
									<span class="play-video" data-video="<?= getYoutubeId($video['video_link']); ?>">
										<img src="<?= ICONS ?>play.png" alt="play">
									</span>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="video-modal">
		<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
			 aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body" id="iframe-wrapper"></div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="close-icon">×</span>
					</button>
				</div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['h_prod_slider']) : ?>
	<section class="products-block">
		<div class="container arrows-slider">
			<div class="row justify-content-center">
				<?php if ($fields['h_prod_slider_title']) : ?>
					<h2 class="base-title">
						<?= $fields['h_prod_slider_title']; ?>
					</h2>
				<?php endif; ?>
				<div class="col-12">
					<?php get_template_part('views/partials/content', 'product_slider',
							[
									'products' => $fields['h_prod_slider'],
							]);
					?>
				</div>
			</div>
			<?php if ($fields['h_prod_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto my-3">
						<a href="<?= $fields['h_prod_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_prod_link']['title']) && $fields['h_prod_link']['title'])
									? $fields['h_prod_link']['title'] : lang_text(['he' => 'עברו לחנות', 'en' => 'To the shop'], 'he');
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['single_slider_seo'],
					'img' => $fields['slider_img'],
			]);
}
get_template_part('views/partials/repeat', 'partners');
get_footer(); ?>
