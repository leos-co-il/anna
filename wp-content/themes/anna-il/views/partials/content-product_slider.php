<?php

if(!isset($args['products']))
	return;

?>

<div class="product-slider" dir="rtl">
	<?php foreach ($args['products'] as $_p){
		echo '<div class="slide-item">';
		foreach ($_p as $post) {
			setup_postdata($GLOBALS['post'] =& $post);
			wc_get_template_part( 'content', 'product' );
		}
		echo '</div>';
	}
	wp_reset_postdata();

	?>
</div>
