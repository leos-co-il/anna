<div class="repeat-form-block" <?php if ($img = opt('base_form_back')) : ?>
	style="background-image: url('<?= $img['url']; ?>')"
<?php endif; ?>>
	<div class="overlay-form">
	</div>
	<div class="container form-container-top">
		<div class="row justify-content-center form-wrapper">
			<div class="col-xl-10 col-lg-12 col-sm-10 col-11">
				<?php if ($title = opt('base_form_title')) : ?>
					<h2 class="form-title"><?= $title; ?></h2>
				<?php endif;
				if ($subtitle = opt('base_form_subtitle')) : ?>
					<h3 class="form-subtitle"><?= $subtitle; ?></h3>
				<?php endif; ?>
				<div class="base-form-wrap">
					<?php getForm('22'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
