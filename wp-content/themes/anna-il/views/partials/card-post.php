<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-4 col-sm-6 col-12 post-col post-col-base">
		<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-item-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
				<span class="post-img-overlay"></span>
			</a>
			<div class="post-card-content-wrap">
				<div class="post-item-content">
					<h2 class="post-item-title"><?= $args['post']->post_title; ?></h2>
					<p class="post-item-text">
						<?= text_preview($args['post']->post_content, 20); ?>
					</p>
				</div>
				<a href="<?= $link; ?>" class="post-link">
					<?= lang_text(['he' => 'המשיכו קריאה', 'en' => 'Keep reading'], 'he'); ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
