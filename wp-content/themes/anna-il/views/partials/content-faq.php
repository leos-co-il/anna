<?php if (isset($args['faq']) && $args['faq']) :
	$title = lang_text(['he' => 'שאלות ותשובות נפוצות - אתם שואלים אחנו עונים', 'en' => 'Frequently Asked Questions - You ask we answer'], 'he'); ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="base-title mb-4">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : $title; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-md-11 col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="faq-icon">
											<img src="<?= ICONS ?>faq.png" alt="question">
										</span>
										<span class="question-title-q">
											<?= $item['faq_question']; ?>
										</span>
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="base-output slider-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
