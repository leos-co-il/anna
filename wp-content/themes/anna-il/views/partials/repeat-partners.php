<?php if ($partners = opt('clients_logos')) :
	$title = opt('clients_title'); ?>
	<section class="clients-block">
		<div class="container clients-block-title">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="base-title mb-3">
						<?= $title ? $title : lang_text(['he' => 'מבין לקוחותינו/', 'en' => 'Our customers /'], 'he'); ?>
					</h2>
				</div>
			</div>
		</div>
		<div class="clients-line arrows-slider">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-11">
						<div class="partners-slider" dir="rtl">
							<?php foreach ($partners as $partner) : ?>
								<div>
									<div class="client-logo">
										<img src="<?= $partner['url']; ?>" alt="customer-logo">
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
