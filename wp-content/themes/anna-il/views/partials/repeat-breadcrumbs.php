<?php if ( function_exists('yoast_breadcrumb')) : ?>
	<div class="container-fluid pt-2 mb-4">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-11 col-12">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
