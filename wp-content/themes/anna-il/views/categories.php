<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'parent' => 0,
]);
?>

<article class="article-page-body page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs');
	if ($cats) : ?>
		<div class="cats-page mb-5">
			<div class="container pt-cont">
				<div class="row justify-content-center">
					<div class="col">
						<h1 class="block-title mb-3"><?php the_title(); ?></h1>
						<div class="base-output text-center mb-3">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach($cats as $number => $parent_term) :
						$thumbnail_id = get_term_meta($parent_term->term_id, 'thumbnail_id', true );
						$cat_image = wp_get_attachment_url( $thumbnail_id ); ?>
						<div class="col-md-6 col-sm-11 col-12 main-cat-col">
							<a class="main-cat-item" href="<?= get_term_link($parent_term); ?>">
								<span class="main-cat-img" <?php if ($cat_image) : ?>
									style="background-image: url('<?= $cat_image; ?>')"
								<?php endif; ?>></span>
								<span class="h-cat-name-wrap">
									<span class="h-cat-name-title">
										<?= $parent_term->name; ?>
									</span>
								</span>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'form');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
get_footer(); ?>
