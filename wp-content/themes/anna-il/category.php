<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
		'posts_per_page' => 6,
		'post_type' => 'post',
		'suppress_filters' => false,
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
$published_posts = get_posts([
		'numberposts' => -1,
		'post_type' => 'post',
		'suppress_filters' => false,
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
?>

<article class="article-page-body page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pt-cont">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title"><?= $query->name; ?></h1>
				<div class="base-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($posts->have_posts()) : ?>
		<div class="container">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts->posts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
						'post' => $post,
					]);
				} ?>
			</div>
		</div>
	<?php endif;
	if ($published_posts && count($published_posts) > 6) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link-cat load-more-posts" data-type="post" data-term="<?= $query->term_id; ?>" data-all="<?= $published_posts; ?>">
						<img src="<?= ICONS ?>arrow-down.png" alt="load-more">
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'form');
if ($faq = get_field('faq_item', $query)) :
	get_template_part('views/partials/content', 'faq',
			[
					'text' => get_field('faq_text', $query),
					'faq' => $faq,
			]);
endif;
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $slider,
					'img' => get_field('slider_img', $query),
			]);
}
get_template_part('views/partials/repeat', 'partners');
get_footer(); ?>
