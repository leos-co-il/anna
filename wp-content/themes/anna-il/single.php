<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body mb-5">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pt-cont">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title post-block-title">
					<?php the_title(); ?>
				</h1>
			</div>
			<div class="col-12 arrows-slider arrows-slider-base post-gallery-arrows">
				<?php if (has_post_thumbnail() && !$fields['post_gallery']) : ?>
					<div class="post-image-wrap">
						<img src="<?= postThumb(); ?>" alt="post-image">
					</div>
				<?php elseif ($fields['post_gallery']): ?>
					<div class="post-slider-wrap">
						<h3 class="block-title post-block-title mb-3">
							<?= lang_text(['he' => 'גלריית תמונות', 'en' => 'Gallery of images'], 'he'); ?>
						</h3>
						<div class="base-slider" dir="rtl">
							<?php if (has_post_thumbnail()) : ?>
							<div class="slider-item p-3">
								<a href="<?= postThumb(); ?>" class="post-gallery-img" style="background-image: url('<?= postThumb(); ?>')"
								   data-lightbox="gallery-post">
								</a>
							</div>
							<?php endif;
							foreach ($fields['post_gallery'] as $img) : ?>
								<div class="slider-item p-3">
									<a href="<?= $img['url']; ?>" class="post-gallery-img" style="background-image: url('<?= $img['url']; ?>')"
									   data-lightbox="gallery-post">
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
}
get_template_part('views/partials/repeat', 'partners');
get_footer(); ?>
