<?php

the_post();
get_header();
$fields = get_fields();

?>
<article class="article-page-body page-body">
	<div class="container pt-cont">
		<div class="row">
			<div class="col-12">
				<h2 class="base-title"><?php the_title(); ?></h2>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'partners');
get_footer(); ?>
