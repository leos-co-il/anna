<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
global $wp_query;
$id = '';
if (is_shop()) {
	$query = wc_get_page_id('shop');
	$image = has_post_thumbnail($query) ? postThumb($query) : '';
} else {
	$query_curr = get_queried_object();
	$query = $query_curr;
	$thumbnail_id = get_term_meta( $query->term_id, 'thumbnail_id', true );
	$image = wp_get_attachment_url( $thumbnail_id );
	$id = $query->term_id;
}
$video = get_field('shop_video', $query);
$terms = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'parent' => 0,
]);
get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
	<section class="top-archive">
		<div class="title-wrap">
			<?php if ($terms) : ?>
			<div class="container pt-4">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($terms as $term) : ?>
						<div class="col-auto">
							<a class="shop-category-link <?= ($id && $id === $term->term_id) ? 'cat-current' : ''; ?>" href="<?= get_term_link($term);?>">
								<?= $term->name; ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="container pt-cont">
				<div class="row justify-content-center">
					<div class="col-xl-10 col-12">
						<div class="row justify-content-between">
							<div class="<?= $video ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?>">
								<h1 class="block-title post-block-title"><?php woocommerce_page_title(); ?></h1>
								<div class="base-output post-output">
									<?php
									/**
									 * Hook: woocommerce_archive_description.
									 *
									 * @hooked woocommerce_taxonomy_archive_description - 10
									 * @hooked woocommerce_product_archive_description - 10
									 */
									do_action( 'woocommerce_archive_description' );
									?>
								</div>
							</div>
							<?php if ($video) : ?>
								<div class="col-lg-6 col-12 video-col-shop">
									<div class="video-item" style="background-image: url('<?= getYoutubeThumb($video)?>')">
										<div class="put-video-here"></div>
									<span class="play-button" data-id="<?= getYoutubeId($video); ?>">
										<img src="<?= ICONS ?>play.png" alt="play">
									</span>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="row justify-content-between">
					<div class="col-12">
						<h3 class="sort-title"><?= lang_text(['he' => 'מיינו מוצרים:', 'en' => 'Sort products:'], 'he'); ?></h3>
					</div>
					<div class="col-auto col-sorting">
						<?php do_action('woocommerce_before_shop_loop'); ?>
					</div>
					<div class="col-auto mt-2">
						<?php do_action('woocommerce_custom_pag'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="container cont-prods-top mb-4">
		<div class="row">
			<div class="col-12">
				<div class="woo-notice block-title">
					<?php woocommerce_output_all_notices() ?>
				</div>
			</div>
		</div>
		<?php
		if ( woocommerce_product_loop() ) {
			woocommerce_product_loop_start();
			echo '<div class="row query-product-list justify-content-center align-items-stretch">';
			if ( wc_get_loop_prop( 'total' ) ) {
				while ( have_posts() ) {
					the_post();

					/**
					 * Hook: woocommerce_shop_loop.
					 */
					do_action( 'woocommerce_shop_loop' );

					echo '<div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb-4 product-item-col show-filter">';
					wc_get_template_part( 'content', 'product' );
					echo '</div>';
				}
			}
			echo '<div>';
			woocommerce_product_loop_end();

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		} else {
			/**
			 * Hook: woocommerce_no_products_found.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action( 'woocommerce_no_products_found' );
		}
		?>
	</div>
<?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq', [
			'title' => get_field('faq_title', $query),
			'faq' => $faq
	]);
}
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider',
			[
					'img' => get_field('slider_img', $query),
					'content' => $slider,
			]);
}
get_footer( 'shop' );
