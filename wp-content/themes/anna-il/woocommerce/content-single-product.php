<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_link = get_the_permalink();
$post_id = $product->get_id();
$fields = get_fields();
$category = get_the_terms($post_id, 'product_cat');
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($post_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
	}
}
$categories = wp_get_object_terms($post_id, 'product_cat', ['fields' => 'ids']);
$cat_title = '';
if ($fields['same_produts_link']) {
	$cat_link = isset($fields['same_produts_link']['url']) ? $fields['same_produts_link']['url'] : '';
	$cat_title = isset($fields['same_produts_link']['title']) ? $fields['same_produts_link']['title'] : '';
}
elseif ($categories) {
	$cat_name = get_term($categories['0']);
	$cat_link = get_term_link($categories['0']);
	$cat_title = $cat_name->name;
}
$samePosts = $fields['same_products'] ? $fields['same_products'] : get_posts([
		'posts_per_page' => 4,
		'post_type' => 'product',
		'post__not_in' => array($post_id),
		'suppress_filters' => false,
		'tax_query' => [
				[
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
						'terms' => $categories,
				],
		],
]);
if (!$samePosts) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'post_type' => 'product',
			'suppress_filters' => false,
			'post__not_in' => array($post_id)
	]);
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<div class="prod-body-back">
		<div class="container product-main-body">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-start row-prod-title-mob">
						<div class="col">
							<h2 class="block-title post-block-title mb-3 mob-prod-title"><?= $product->get_title(); ?></h2>
						</div>
					</div>
					<div class="row justify-content-between align-items-start">
						<div class="col-xl-5 col-lg-6 col-12 product-page-info-col">
							<h1 class="block-title post-block-title desctop-title"><?= $product->get_title(); ?></h1>
							<div class="price-custom">
								<?php do_action('woocommerce_single_product_price_custom'); ?>
							</div>
							<div class="summary entry-summary">
								<?php
								/**
								 * Hook: woocommerce_single_product_summary.
								 *
								 * @hooked woocommerce_template_single_title - 5
								 * @hooked woocommerce_template_single_rating - 10
								 * @hooked woocommerce_template_single_price - 10
								 * @hooked woocommerce_template_single_excerpt - 20
								 * @hooked woocommerce_template_single_add_to_cart - 30
								 * @hooked woocommerce_template_single_meta - 40
								 * @hooked woocommerce_template_single_sharing - 50
								 * @hooked WC_Structured_Data::generate_product_data() - 60
								 */
								do_action( 'woocommerce_single_product_summary' );
								?>
							</div>
							<div class="socials-share">
					<span class="base-text share-text">
						<?= lang_text(['he' => 'שתפו מוצר', 'en' => 'Share product'], 'he'); ?>
					</span>
								<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
								   class="social-share-link">
									<img src="<?= ICONS ?>share-facebook.png">
								</a>
								<!--	WHATSAPP-->
								<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
									<img src="<?= ICONS ?>share-whatsapp.png">
								</a>
								<!--	MAIL-->
								<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
								   class="social-share-link">
									<img src="<?= ICONS ?>share-email.png">
								</a>
							</div>
						</div>
						<div class="col-lg-6 product-page-gal-col">
							<div class="gallery-slider-wrap">
								<div class="thumbs">
									<?php if($product_thumb): ?>
										<div class="pad-3">
											<a class="thumb-item" href="<?= $product_thumb; ?>" data-lightbox="images-small">
												<img src="<?= $product_thumb; ?>" alt="product-img">
											</a>
										</div>
									<?php endif;
									foreach ($product_gallery_images as $img): ?>
										<div class="pad-3">
											<a class="thumb-item" href="<?= $img; ?>" data-lightbox="images-small">
												<img src="<?= $img; ?>" alt="gallery-img">
											</a>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="gallery-slider" dir="rtl">
									<?php if($product_thumb): ?>
										<div class="pad-3">
											<a class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')"
											   href="<?= $product_thumb; ?>" data-lightbox="images">
									<span class="gallery-trigger">
									</span>
												<?php if ( $product->is_on_sale() ) : ?>

													<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>

												<?php endif; ?>
											</a>
										</div>
									<?php endif;
									foreach ($product_gallery_images as $img): ?>
										<div class="pad-3">
											<a class="big-slider-item" style="background-image: url('<?= $img; ?>')"
											   href="<?= $img; ?>" data-lightbox="images">
									<span class="gallery-trigger">
									</span>
											</a>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' ); ?>
	</div>
	<?php
	if ($samePosts) : ?>
		<section class="related-block my-5">
			<div class="related-title-wrap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<h2 class="block-title">
								<?= $fields['same_products_title'] ? $fields['same_products_title'] : lang_text(['he' => 'מוצרים דומים:', 'en' => 'Other products that may interest you /'], 'he'); ?>
							</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($samePosts as $_p){
						echo '<div class="col-lg-3 col-sm-6 col-12 mb-4">';
						setup_postdata($GLOBALS['post'] =& $_p);
						wc_get_template_part( 'content', 'product' );
						echo '</div>';
					}
					wp_reset_postdata();?>
				</div>
				<?php if ($cat_link) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<a href="<?= $cat_link; ?>" class="base-link">
								<?= lang_text(['he' => 'חזרו ל', 'en' => 'Return to'], 'he').$cat_title; ?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</section>
	<?php endif;
	?>
</div>
<?php do_action( 'woocommerce_after_single_product' );
get_template_part('views/partials/repeat', 'form');
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
			]);
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['single_slider_seo'],
					'img' => $fields['slider_img'],
			]);
}?>
