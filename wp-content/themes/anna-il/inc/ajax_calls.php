<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

	if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
		exit("No naughty business please");
	}

	$result['type'] = "error";
	$result['var'] = 1;

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function()
{
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$quantity = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$ids = explode(',', $ids_string);
	$html = '';
	$result['html'] = '';
	$result['quantity'] = '';
	$query = new WP_Query([
		'post_type' => 'post',
		'posts_per_page' => 3,
		'post__not_in' => $ids,
		'suppress_filters' => false,
		'tax_query' => $id_term ? [
			[
				'taxonomy' => 'category',
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
	if ($query->have_posts()) {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
		if (wp_count_posts('post')->publish >= ($quantity + 3)) {
			$result['quantity'] = true;
		}
	}
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
add_action('wp_ajax_add_product_to_cart', 'add_product_to_cart');
add_action('wp_ajax_nopriv_add_product_to_cart', 'add_product_to_cart');

function add_product_to_cart() {

	$product_id = isset($_REQUEST['product_id']) ?  intval($_REQUEST['product_id']) : null;
	$quantity = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : null;
	$custom_data = [];
	$_product = wc_get_product( $product_id );


	$cart = WC()->cart;

	if($_POST['delete'] === 'true'){
		$cart->remove_cart_item($product_id);
	}else{
		$cart->add_to_cart( $product_id, $quantity, '0', array(), $custom_data );
	}

	$result['type'] = "success";

//	wc_get_template( 'cart/mini-cart.php' );

	die();
}


function update_cart_count(){
	global $woocommerce;
	echo $woocommerce->cart->cart_contents_count;
}

add_action('wp_ajax_nopriv_update_cart_count', 'update_cart_count');
add_action('wp_ajax_update_cart_count', 'update_cart_count');
