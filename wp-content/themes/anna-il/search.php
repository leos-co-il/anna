<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block page-body pb-5">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args_1 = array(
			'post_type' => 'post',
			's' => $s
		);
		$args_2 = array(
			'post_type' => 'product',
			's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		$the_query_2 = new WP_Query( $args_2 );
		if ( $the_query_1->have_posts() ) { ?>
		<h4 class="base-title my-3"><?= lang_text(['he' => 'תוצאות חיפוש עבור:', 'en' => 'Search Results for:'], 'he').get_query_var('s') ?></h4>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink(); ?>
				<div class="col-lg-4 col-md-6 col-12 post-col post-col-base">
					<div class="post-item more-card">
						<a class="post-item-image" href="<?= $link; ?>"
							<?php if (has_post_thumbnail()) : ?>
								style="background-image: url('<?= postThumb(); ?>')"
							<?php endif;?>>
							<span class="post-img-overlay"></span>
						</a>
						<div class="post-card-content-wrap">
							<div class="post-item-content">
								<h2 class="post-item-title"><?php the_title(); ?></h2>
								<p class="post-item-text">
									<?= text_preview(get_the_content(), 20); ?>
								</p>
							</div>
							<a href="<?= $link; ?>" class="post-link">
								<?= lang_text(['he' => 'המשיכו קריאה', 'en' => 'Keep reading'], 'he'); ?>
							</a>
						</div>
					</div>
				</div>
			<?php }
			} ?>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php if ( $the_query_2->have_posts() ) {
				while ( $the_query_2->have_posts() ) { $the_query_2->the_post(); ?>
					<div class="col-lg-4 col-md-6 col-12 mb-4">
						<?php
						$post_object = get_post( get_the_ID());

						setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

						wc_get_template_part( 'content', 'product' );
						?>
					</div>
				<?php }
			} ?>
		</div>
		<?php if (!$the_query_1->have_posts() && !$the_query_2->have_posts()) : ?>
			<div class="col-12 pt-5">
				<h4 class="base-title">
					<?= lang_text(['he' => 'שום דבר לא נמצא', 'en' => 'Nothing was found'], 'he'); ?>
				</h4>
			</div>
			<div class="alert alert-info text-center mt-5">
				<p>
					<?= lang_text(['he' => 'מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.', 'en' => 'Sorry, nothing matched your search criteria. Please try again with different keywords.'], 'he'); ?>
				</p>
			</div>
		<?php endif; ?>
	</div>
</div>
</div>
</div>
<?php get_footer(); ?>
